export const environment = {
  baseUrl: 'https://app.airspacenter.com/api/user/',
  map_key: 'AIzaSyBqPcPUJFsC_gzl-q-l5loUOCZAP1bLvJ4',
  production: true,
  localization: {
    languages: [
      { code: 'en', name: 'EN', culture: 'en-EN' },
      { code: 'vi', name: 'VN', culture: 'vi-VN' }
    ],
    defaultLanguage: 'vi'
  }
};
