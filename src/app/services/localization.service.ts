import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocalizationService {
  public defaultLanguage: string;
  public supportedLanguage: string[];
  constructor(
    private translateService: TranslateService
  ) { }

  public init() {
    const localization: any = environment.localization;
    const languages: Array<string> = localization.languages.map(lang => lang.code);

    this.supportedLanguage = languages;
    this.defaultLanguage = !localStorage.getItem('language') ? localization.defaultLanguage : localStorage.getItem('language');

    this.translateService.addLangs(languages);
    this.translateService.setDefaultLang(this.defaultLanguage);

    console.log(`[Language] Init: ${this.defaultLanguage}`);
    this.translateService.use(this.defaultLanguage);
  }

  public get language(): string {
    return this.translateService.currentLang;
  }

  public set language(language: string) {
    const isSupportedLanguage: boolean = this.supportedLanguage.find(lang => lang === language) !== null;
    if (!isSupportedLanguage) {
      language = this.defaultLanguage;
    }
    console.log(`[Language] Set language: ${language}`);
    this.translateService.use(language);
  }
}
