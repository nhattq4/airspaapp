import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocalizationService } from './localization.service';
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  baseUrl: any = environment.baseUrl;
  category: any;
  userToken: any;
  deviceToken: any;
  verifyMo: any;
  // tslint:disable-next-line:variable-name
  phone_no: any;
  id: any;
  bookid: any;
  time: any = {};
  private currentLanguage = this.localization.defaultLanguage;
  constructor(
    private http: HttpClient,
    private localization: LocalizationService
  ) {
    if (localStorage.getItem('token')) {
      this.userToken = localStorage.getItem('token');
    }
  }
  getData(url) {
    this.currentLanguage = this.localization.language;
    return this.http.get(this.baseUrl + `${url}?lang=${this.currentLanguage}`);
  }
  postData(url, data) {
    this.currentLanguage = this.localization.language;
    Object.assign(data, { lang: this.currentLanguage });
    return this.http.post(this.baseUrl + url, data);
  }

  getDataWithToken(url) {
    this.currentLanguage = this.localization.language;
    let header = new HttpHeaders();
    header = header.set('Authorization', 'Bearer ' + this.userToken);
    header = header.set('Accept', 'application/json');
    return this.http.get(this.baseUrl + `${url}?lang=${this.currentLanguage}`, { headers: header });
  }

  postDataWithToken(url, data) {
    this.currentLanguage = this.localization.language;
    Object.assign(data, { lang: this.currentLanguage });
    let header = new HttpHeaders();
    header = header.set('Authorization', 'Bearer ' + this.userToken);
    header = header.set('Accept', 'application/json');
    return this.http.post(this.baseUrl + url, data, { headers: header });
  }
}
