import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageCacheService {
  private cache = new Map<string, string>();
  constructor(
    public translateService: TranslateService
  ) { }

  get(key: string): string {
    const value = this.cache.get(key);
    if (value) {
      return value;
    } else {
      this.translateService.stream(key).subscribe(trans => {
        this.cache.set(key, trans);
      });
      return this.cache.get(key);
    }
  }
}
