import { TestBed } from '@angular/core/testing';

import { LanguageCacheService } from './language-cache.service';

describe('LanguageCacheService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LanguageCacheService = TestBed.get(LanguageCacheService);
    expect(service).toBeTruthy();
  });
});
