import { UtilService } from './../../services/util.service';
import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { LanguageListPage } from '../language-list/language-list.page';
import { LocalizationService } from 'src/app/services/localization.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  data: any = {};
  err: any = {};

  constructor(
    private modalCtrl: ModalController,
    private navCtrl: NavController,
    private api: ApiService,
    private util: UtilService,
    protected language: LocalizationService
  ) {
    this.util.startLoad();
    this.api.getDataWithToken('profile').subscribe(
      (res: any) => {
        this.util.dismissLoader();
        this.data = res;
        if (this.data.noti) {
          this.data.noti = true;
        } else {
          this.data.noti = false;
        }
      },
      (err) => {
        this.util.dismissLoader();
        this.err = err.error.errors;
      }
    );
  }

  ngOnInit() { }

  viewPage(path) {
    this.navCtrl.navigateForward(path);
  }
  dataChanged() {
    const info: any = {};
    console.log(this.data.noti);

    if (this.data.noti) {
      info.noti = 0;
    } else {
      info.noti = 1;
    }

    this.util.startLoad();
    this.api.postDataWithToken('profile/update', info).subscribe(
      (res: any) => {
        if (res.success) {
          this.api.getDataWithToken('profile').subscribe(
            (response: any) => {
              this.util.dismissLoader();
              this.data = response;
            },
            (err) => {
              this.util.dismissLoader();
              this.err = err.error.errors;
            }
          );

          this.util.presentToast('Profile is updated');
        } else {
          this.util.presentToast(res.message);
        }
      },
      (err) => {
        this.err = err.error.errors;
      }
    );
  }
  async presentTopratedModal() {
    const modal = await this.modalCtrl.create({
      component: LanguageListPage,
      backdropDismiss: true,
      cssClass: 'sort-modal',
    });
    modal.onDidDismiss().then((data) => {
      if (data.data !== undefined) {
        this.language.language = data.data.toString();
      }
    });
    return await modal.present();
  }
}
