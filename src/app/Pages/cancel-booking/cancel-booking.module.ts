import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CancelBookingPageRoutingModule } from './cancel-booking-routing.module';

import { CancelBookingPage } from './cancel-booking.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CancelBookingPageRoutingModule,
    TranslateModule
  ],
  declarations: [CancelBookingPage],
  exports: [CancelBookingPage]
})
export class CancelBookingPageModule { }
