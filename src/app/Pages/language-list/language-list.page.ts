import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-language-list',
  templateUrl: './language-list.page.html',
  styleUrls: ['./language-list.page.scss'],
})
export class LanguageListPage implements OnInit {
  defaultLanguage = 'vi';
  languageList: any = [
    { title: 'English', code: 'en' },
    { title: 'Vietnam', code: 'vi' }
  ];
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }
  closeModal() {
    this.modalCtrl.dismiss();
  }
  filter(val) {
    localStorage.setItem('language', val);
    this.modalCtrl.dismiss(val);
  }
}
